def physic_ok(behavior):
    physic = input("What is the physical equivalent of {}?\n".format(behavior))
    physic_op = input('Do you consider the behavior "{}" ok? please write "yes" or "no"\n'.format(physic))
    return physic_op.lower() == 'yes'


def would_you_like_it(behavior):
    answer = input(" would you like that same behavior, {} to affect you? please write 'yes' or 'no'\n".format(behavior))
    return answer == "yes"


def main():
    behavior = input("Welcome to the moral decision making assistant! What is the behavior in question?\n")
    physic_answer = physic_ok(behavior)
    if not(physic_answer):
        print ("According to our analysis, you should not to it. Try to find other ways to solve your problem.")
    else:
        would_you_like_it = would_you_like_it(behavior)
        if would_you_like_it:
            print ("According to our analysis, you should do it.\nHave a great day!")
        else:
            print ("According to our analysis, you should not to it. Try to find other ways to solve your problem.")


if __name__ == "__main__":
    main()
