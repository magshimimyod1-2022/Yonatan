import datetime
import socket

SERVER_IP = "34.218.16.79"
SERVER_PORT = 77

def menu():
    menu_string = """\
        1- What's the weather today
        2- What's the weather of today and the next 3 days """
    city = input("What is your city? ")
    print(menu_string)

    choise = int(input())
    if choise == 1:
        print(check_weather(city, datetime.datetime.now().strftime("%d/%m/%Y")))
    elif choise == 2:
        for i in range(4):
            date = (datetime.datetime.now() + datetime.timedelta(days=i)).strftime("%d/%m/%Y")
            temp, description = check_weather(city, date)
            print(("{}, Temperature: {}, {}").format(date, temp, description))


def check_weather(city, date):
    alphabet_gimatria = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8, "i":9, "j":10, "k":11, "l":12, "m":13, 
    "n":14, "o":15, "p":16, "q":17, "r":18, "s":19, "t":20, "u":21, "v":22, "w":23, "x":24, "y":25, "z":26, " ":0}

    checksum1 = 0
    for i, char in enumerate(city.lower()):
        checksum1 += alphabet_gimatria[char]
    
    checksum2 = 0 
    for i in date:
        if i != "/" :
            checksum2 += int(i)

    MSG = "100:REQUEST:city={}&date={}&checksum={}.{}".format(city, date, checksum1, checksum2)
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        server_msg = sock.recv(1024)
        sock.sendall(MSG.encode())
        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()

        if int(server_msg[:3]) == 200:
            temp, description = float(server_msg.split("&")[2][5:]), server_msg.split("&")[3][5:]
            return(temp, description)
        else:
            return(999, server_msg[10:])


    
def main():
    menu()
    


if __name__ == "__main__":
    main()
