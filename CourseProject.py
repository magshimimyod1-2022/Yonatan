

def print_hangman_opening():
    """print hangman opening
    :return: none
    :rtype: none
    """
    HANGMAN_ASCIIART = """\

    _    _                                         
    | |  | |                                        
    | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
    |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
    | |  | | (_| | | | | (_| | | | | | | (_| | | | |
    |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |                      
                        |___/                      
        """
    print(HANGMAN_ASCIIART)

def show_hidden_word(secret_word, old_guessed_letters):
    """shows hidden word
    :param secret_word: the secret letter
    :type secret_word: string
    :return: none
    :rtype: none
    """
    hidden_word = ''
    for letter in secret_word:
        if letter in old_guessed_letters:
            hidden_word += letter
        else:
            hidden_word += "_ "
    print(hidden_word)
    
def is_valid_input(letter_guessed, old_letters_guessed):
    """Tells you whether your input is a valid input
   :param letter_guessed: guessed letter
   :type letter_guessed: string
   :return: If the input is valid
   :rtype: bool
   """
    if len(letter_guessed) > 1 or not(letter_guessed.isalpha()) or (letter_guessed.lower() in old_letters_guessed):
        return False
    return True

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """Try to update the letter guessed
    :param letter_guessed: guessed letter
    :param old_letters_guessed: already guessed letters
    :type letter_guessed: string
    :type old_letters_guessed: list
    :return: if the guessed letter was updated / X, what letters were already guessed and if it was updated
    :rtype: bool / string + bool
    """
    if not(is_valid_input(letter_guessed, old_letters_guessed)):
        print("X\n" + " -> ".join(sorted(old_letters_guessed)))
        return False
    old_letters_guessed.append(letter_guessed)
    return True

def did_guess_right(guess, secret_word):
    """did the user guess right
    :param guess: guessed letter
    :param secret_word: secret_word
    :type guess: string
    :type secret_word: str
    :return: if the guess was right
    :rtype: bool
    """
    return (guess in secret_word)
def check_win(secret_word, old_letters_guessed):
    """if we won
    :param secret_word: the secret word
    :param old_letters_guessed: the letters we gueesed
    :type secret_word: str
    :type old_letters_guessed: list
    :return: if we won
    :rtype: bool
    """
    return set(secret_word).issubset(set(old_letters_guessed))

def print_hangman(num_of_tries):
    """print hangman by number of tries
    :param num_of_tries: number of tries
    :type num_of_tries: int
    :return: none
    :rtype: none
    """
    HANGMAN_PHOTOS = {"picture 1": """x-------x """, "picture 2": """x-------x\n|\n|\n|\n|\n|""", "picture 3": """x-------x\n|       |\n|       0\n|\n|\n|""", "picture 4": """x-------x\n|       |\n|       0\n|       |\n|\n|""", "picture 5": """x-------x\n|       |\n|       0\n|      /|\\\n|\n|""", "picture 6": """x-------x\n|       |\n|       0\n|      /|\\ \n|      / \n|""", "picture 7": """x-------x\n|       |\n|       0\n|      /|\\\n|      / \\\n|"""}
    print(HANGMAN_PHOTOS["picture " + str(num_of_tries)])

def choose_word(file_path, index):
    """choose a word
    :param file_path: the file to choose word from
    :param index: the index of the word
    :type file_path: str
    :type index: int
    :return: amount of non repeating words and the word choosen
    :rtype: tuple
    """
    words = list()
    with open(file_path,'r') as file:
   
        for line in file:
            for word in line.split():
                words.append(word) 

    non_repeated = set(words)
    if len(words) < index:
        index = index % len(words)
    word = words[index - 1]
    return (word)


def main():
    max_tries = 7
    exit = False
    num_of_tries = 1
    
    secret_word_path = input()
    secret_word_index = int(input())
    secret_word = choose_word(secret_word_path, secret_word_index)
    print_hangman_opening()
    old_guesses = list()
    show_hidden_word(secret_word, old_guesses)
    while not(exit):
        print_hangman(num_of_tries)
        guess = input()
        if did_guess_right(guess, secret_word) == False:
            max_tries -= 1
            num_of_tries += 1
        try_update_letter_guessed(guess, old_guesses)
        show_hidden_word(secret_word, old_guesses)
        if max_tries <= 0:
            exit = True
            print('LOSE')
        elif check_win(secret_word, old_guesses):
            exit = True
            print('WON')

if __name__ == "__main__":
    main()
