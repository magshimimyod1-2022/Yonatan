import socket

SERVER_IP = "127.0.0.1"
SERVER_PORT = 65432
MENU = """
1 - get album list 
2 - get album songs
3 - get song length
4 - get song lyrics
5 - get song album
6 - find song
7 - find song by lyrics
8 - exit
"""

REQUESTS = {"100": "100:REQUEST:command=get_album_list&param=", "200": "200:REQUEST:command=get_album_songs&param=", 
            "300": "300:REQUEST:command=get_song_length&param=", "400": "400:REQUEST:command=get_song_lyrics&param=", 
            "500": "500:REQUEST:command=get_song_album&param=", "600": "600:REQUEST:command=find_song_name&param=", 
            "700": "700:REQUEST:command=find_song_by_lyrics&param=", "800": "800:REQUEST:command=close_connection&param="}

PARAMS = {"1": "album name:", "2": "song name:", "3": "song name:", "4": "song name:", "5": "song name:", "6": "song name:", "7": "lyrics:"}
            
def client():
    "a client"
    Quit = False

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        server_address = (SERVER_IP, SERVER_PORT)
        try:
            sock.connect(server_address)
        except:
            print("Server is not connecting!!!")
        server_msg = sock.recv(1024).decode()
        print(server_msg)
        while not Quit:
            
            print(MENU)
            choise = int(input("Please enter choise:"))
            if choise != 1 and choise != 8:
                param = input("Please enter " + PARAMS[str(choise)])

            if(choise != 8 ):
                try:
                    sock.sendall((REQUESTS[str(choise * 100)] + param).encode())
                    server_msg = sock.recv(1024)
                    print("action:" + server_msg.decode())
                except:
                    print("Server is not responding!!!")
                    Quit = True
            else:
                Quit = True

def main():
    client()
    


if __name__ == "__main__":
    main()
